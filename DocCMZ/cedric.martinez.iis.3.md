---
title: SRW3 - Documentation IIS Partie 3
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\iis.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

## Prérequis

Afin de pouvoir suivre le guide d'installation, il est nécessaire de disposer d'un hyperviseur ainsi que d'une image ISO de Windows Server 2016. Le guide sera effectué en utilisant l^hyperviseur VMware Workstation. La machine virtuelle devra avoir été créée au préalable en utilisant les paramètres par défaut.

\newpage

# Configuration du site

## Création des répertoires

Afin de pouvoir créer les sites IIS, il faut tout d'abord créer les dossiers dans lesquels ils se situeront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le dossier du Labo.

```Powershell
New-Item -ItemType directory -Path C:/SRW/Labo
```

Il faut ensuite saisir la commande suivante afin de créer le dossier privé du Labo.

```Powershell
New-Item -ItemType directory -Path C:/SRW/Labo/private
```

Il faut ensuite saisir la commande suivante afin de créer le dossier public du Labo.

```Powershell
New-Item -ItemType directory -Path C:/SRW/Labo/public
```

## Création des fichiers html

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer les fichiers html qu'ils contiendront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier labo.html du site Labo.

```Powershell
New-Item -ItemType File -Path C:/SRW/Labo/labo.html
```

Il faut ensuite saisir la commande suivante afin de créer le fichier private.html du site privateLabo.

```Powershell
New-Item -ItemType File -Path C:/SRW/Labo/private/private.html
```

Il faut ensuite saisir la commande suivante afin de créer le fichier public.html du site publicLabo.

```Powershell
New-Item -ItemType File -Path C:/SRW/Labo/public/public.html
```

## Création du site IIS

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer le site IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le site IIS labo du Module SRW.

```Powershell
C:\Windows\system32\inetsrv\appcmd add site /name:"labo du Module SRW"
/id:41 /physicalPath:"C:\SRW\labo" /bindings:http/*:80:
```

Il faut ensuite saisir la commande suivante afin de créer le Pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd add apppool /name:"labo du Module SRW"
```

Il faut ensuite saisir la commande suivante afin de définir le pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd set app "labo du Module SRW/"
/applicationPool:"labo du Module SRW"
```

Il faut ensuite saisir la commande suivante afin de définir le document par défaut du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd set config "labo du Module SRW"
/section:defaultDocument /+files.[@start,value='labo.html']
```

Il faut ensuite saisir la commande suivante afin de définir le document par défaut du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd set config "labo du Module SRW/private"
/section:defaultDocument "/+files.[@start,value='private.html']"
```

Il faut ensuite saisir la commande suivante afin de définir le document par défaut du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd set config "labo du Module SRW/public"
/section:defaultDocument "/+files.[@start,value='public.html']"
```

Il faut ensuite saisir la commande suivante afin d'activer le listing des dossiers.

```Powershell
C:\Windows\system32\inetsrv\appcmd set config "labo du Module SRW"
/section:directoryBrowse /enabled:True
```

Il faut ensuite saisir la commande suivante afin de démarrer le site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd start site /site.name:"labo du Module SRW"
```

## Tests

Les tests ont été efffectués depuis une machine cliente Windows 10 avec le navigateur Edge dans le domaine AD.

| Test | Résultat |
| :------------- | :------------- |
| Connexion http://IP:80 | labo.html affiché |
| Connexion http://IP/private:80 | private.html affiché |
| Connexion http://IP/public:80 | public.html affiché |


Les tests ont permis de démontrer le bon fonctionnement de la configuration d'IIS.

\newpage

# Configuration des règles d'adresse

## Installation de la fonctionnalité

Afin de pouvoir refuser l'accès aux sites IIS depuis la machine hôte, il faut tout d'abord installer la fonctionnalité IP Restrictions au serveur IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'installer la fonctionnalité nécessaire.

```Powershell
Install-WindowsFeature Web-IP-Security
```

## Adresse IP de l'hôte

Afin de pouvoir refuser l'accès aux sites IIS depuis la machine hôte, il faut tout d'abord connaître l'adresse IP de la machine hôte.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de connaitre l'adresse IP de la machine hôte.

```Powershell
ipconfig /all
```

L'adresse IP récupéré est 192.168.55.1.


## Refus de l'accès au site labo du Module SRW

Afin de pouvoir refuser l'accès aux sites IIS depuis la machine hôte, il faut activer le reverse DNS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'activer le reverse DNS.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "labo du Module SRW"
-section:system.webServer/security/ipSecurity /enableReverseDns:true /commit:apphost
```

Afin de pouvoir refuser l'accès aux sites IIS depuis la machine hôte, il faut ajouter l'adresse IP à refuser.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter l'addresse IP à refuser.


```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "labo du Module SRW"
-section:system.webServer/security/ipSecurity
/+"[ipAddress='192.168.55.1',allowed='False']" /commit:apphost
```

## Tests

Les tests ont été efffectués depuis la machine hôte à l'aide du navigateur Mozilla Firefox.

| Test | Résultat |
| :------------- | :------------- |
| Connexion à http://IP:80 | Erreur 403 |
| Connexion à http://IP:80/public | Erreur 403 |
| Connexion à http://IP:80/private | Erreur 403 |

Les tests ont permis de démontrer le bon fonctionnement de la configuration d'IIS.

\newpage

# Configuration de l'authentification

## Installation de la fonctionnalité

Afin de pouvoir activer l'authentification en utilisant Digest, il faut tout d'abord installer la fonctionnalité Digest Authentication au serveur IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'installer la fonctionnalité Digest Authentication.

```Powershell
Install-WindowsFeature Web-Digest-Auth
```

## Création d'un compte AD SRW_local

Afin de pouvoir utiliser un compte spécifique pour l'authentification anonyme, il faut créer le compte à utiliser.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le compte utilisateur local.

```Powershell
New-ADUser -Name "SRW_local" -GivenName SRW -Surname Local
-SamAccountName SRW_local -UserPrincipalName SRW_local@team11.local
-AccountPassword (Read-Host -AsSecureString "AccountPassword") -PassThru | Enable-ADAccount
```

## Utilisation de SRW_local comme user anonymous

Afin de pouvoir utiliser un compte spécifique pour l'authentification anonyme, il faut configurer IIS pour utiliser le compte créé.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de configurer IIS pour utiliser le compte local créé précédemment.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config /section:anonymousAuthentication
/userName:TEAM11\SRW_local /password:Qwertz123456
```

## Création d'un compte AD SRW_private

Afin de pouvoir utiliser un compte spécifique pour l'accès au site IIS private, il faut créer le compte à utiliser.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le compte utilisateur local utilisé pour l'accès au site ISS private.

```Powershell
New-ADUser -Name "SRW_private" -GivenName SRW -Surname Private
-SamAccountName SRW_private -UserPrincipalName SRW_private@team11.local
-AccountPassword (Read-Host -AsSecureString "AccountPassword") -PassThru | Enable-ADAccount
```

## Tests

Les tests ont été efffectués depuis une machine cliente Windows 10 avec le navigateur Edge dans le domaine AD.

| Test | Résultat |
| :------------- | :------------- |
| Connexion à http://IP:80/public | Accès à public.html |

Les tests ont permis de démontrer le bon fonctionnement de la configuration d'IIS.

## Activation de l'authentification Windows

Afin de pouvoir utiliser l'authentification Windows, il est nécessaire de définir le mode d'authentification du site IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de définir le mode d'authentification Windows sur le site IIS.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/windowsAuthentication"
-Name Enabled -Value True -PSPath "IIS:\sites\labo du Module SRW/private"
```

## Activation de l'authentification Digest

Afin de pouvoir utiliser l'authentification Digest, il est nécessaire de définir le mode d'authentification du site IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de définir le mode d'authentification Digest sur le site IIS.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/digestAuthentication"
-Name Enabled -Value True -PSPath "IIS:\sites\labo du Module SRW/private"
```

## Désactivation de l'authentification anonyme

Afin de pouvoir utiliser les autres modes d'authentification, il est nécessaire de désactiver le mode d'authentification anonyme du site IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de désactiver le mode d'authentification anonyme sur le site IIS.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/anonymousAuthentication"
-Name Enabled -Value False -PSPath "IIS:\sites\labo du Module SRW/private"
```

## Restriction de l'accès à SRW_private

Afin de restrindre l'accès à un compte spécifique au site IIS private, il faut ajouter le compte à autoriser.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de restrindre l'accès à un compte spécifique au site IIS private.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "labo du Module SRW/private"
-section:system.webServer/security/authorization
/+"[accessType='Allow',users='TEAM11\SRW_private']"
```

## Connexion avec IExplore

Afin de tester la configuration du site IIS, il faut tout d'abord se connecter avec l'utilisateur SRW_private et accéder au site.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante en remplaçant IP par l'adresse IP du serveur IIS afin d'ouvrir Internet Explorer.

```Powershell
Iexplore.exe http://IP:80/private
```

La page private.html devrait s'afficher.

## Connexion Digest

Afin de tester la configuration du site IIS, il faut tout d'abord se connecter avec l'utilisateur SRW_private et accéder au site.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante en remplaçant IP par l'adresse IP du serveur IIS afin d'ouvrir Mozilla Firefox.

```Powershell
firefox.exe http://IP:80/private
```

Il faut ensuite saisir TEAM11\\SRW_private dans le champ Username, Qwertz123456 dans le champ Password et cliquer sur le bouton OK.

Finalement, la page private.html devrait s'afficher.

## Tests

Les tests ont été efffectués depuis une machine cliente Windows 10 avec le navigateur Internet Explorer puis avec le navigateur Mozilla Firefox dans le domaine AD.

| Test | Résultat |
| :------------- | :------------- |
| Connexion à http://IP:80/private avec IE | Accès autorisé |
| Connexion à http://IP:80/private avec Firefox | Accès autorisé |  

Les tests ont permis de démontrer le bon fonctionnement de la configuration d'IIS.

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://fr.wikipedia.org/w/index.php?title=Internet_Information_Services&oldid=140016257, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/install/installing-iis-85/installing-iis-85-on-windows-server-2012-r2, consulté le 30.01.2018.

http://www.tomsitpro.com/articles/powershell-manage-iis-websites,2-994.html, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/application-frameworks/install-and-configure-php-on-iis/install-and-configure-php, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/powershell/module/webadminstration/set-webhandler?view=winserver2012-ps&viewFallbackFrom=winserver2012r2-ps, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/powershell/module/webadministration/new-webftpsite?view=win10-ps, consulté le 30.01.2018.

https://blogs.iis.net/bills/how-to-backup-restore-iis7-configuration, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/get-started/getting-started-with-iis/create-a-web-site, consulté le 2 février 2018

https://docs.microsoft.com/en-us/iis/configuration/system.webserver/security/ipsecurity/add, consulté le 13 février 2018

http://techgenix.com/creating-active-directory-accounts-using-powershell/, consulté le 13 février 2018

https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc770966(v=ws.10), consulté le 13 février 2018
\newpage

# Annexes

Ment.
