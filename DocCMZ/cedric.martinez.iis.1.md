---
title: SRW3 - Documentation IIS Partie 1
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\iis.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

## Prérequis

Afin de pouvoir suivre le guide d'installation, il est nécessaire de disposer d'un hyperviseur ainsi que d'une image ISO de Windows Server 2016. Le guide sera effectué en utilisant l^hyperviseur VMware Workstation. La machine virtuelle devra avoir été créée au préalable en utilisant les paramètres par défaut.

\newpage

# Mise en place de l'infrastructure

## Installation de Windows Server 2016

Afin de pouvoir installer le serveur IIS, il faut tout d'abord installer le système d'exploitation Windows Server 2016.

Pour ce faire, il faut insérer l'image d'installation dans la machine virtuelle et démarrer celle-ci.

Puis, il faut sélectionner English (Switzerland) dans le champ Region ainsi que Swiss French dans le champ Keyboard Layout et cliquer sur le bouton Install.

Ensuite, il faut cliquer sur le bouton Customized.

Après cela, il faut cliquer sur le bouton New et sur le bouton Next.

Après l'installation, le serveur redémarre.

## Configuration de Windows Server 2016

Après le redémarrage du serveur, il faut saisir deux fois le mot de passe de l'administrateur local.

Il faut ensuite presser simultanément sur les touches Windows + R, saisir powershell et presser sur Enter.

Ensuite, il faut saisir la commande suivante pour définir l'adresse IP du serveur.

```Powershell
netsh interface ip set address "Ethernet0" static 192.168.55.10 255.255.255.0 192.168.55.1
```

Puis, il faut saisir la commande suivante pour définir l'addresse IP du serveur DNS.

```Powershell
netsh interface ip set dns "Ethernet0" static 192.168.55.5
```

Ensuite, il faut saisir la commande suivante afin de définir le nom d'hôte du serveur.

```Powershell
Rename-Computer -NewName IIS -PassThru -Force
```

Finalement, il faut saisir la commande suivante pour redémarrer le serveur.

```Powershell
Restart-Computer
```

\newpage

# Installation d'IIS

## Installation du rôle IIS

Afin d'installer le rôle IIS, il faut tout d'abord se logguer en tant qu'administrateur local.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'installer le rôle IIS.

```Powershell
Import-Module ServerManager
Install-WindowsFeature Web-Server,Web-Net-Ext45,Web-Asp-Net45,Web-ISAPI-Ext,
Web-ISAPI-Filter,Web-Mgmt-Tools,Web-Mgmt-Console,Web-Mgmt-Service,
Web-CGI,Web-Windows-Auth
```

\newpage

# Configuration des sites IIS

## Désactivation du site par défaut

Afin de désactiver le site IIS par défaut, il faut tout d'abord se logguer en tant qu'administrateur local.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de désactiver le site par défaut.

```Powershell
Get-Website | Remove-Website
```

## Création du site IIS

### Création du dossier

Afin de créer le dossier qui contiendra le site IIS, il faut tout d'abord se logguer en tant qu'administrateur local.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le nouveau dossier.

```Powershell
New-Item -ItemType directory -Path C:\iis_www
```

### Création du site

Afin de créer le nouveau site IIS, il faut tout d'abord se logguer en tant qu'administrateur local.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le nouveau site IIS.

```Powershell
New-Website -Name "MON Site IIS" –PhysicalPath "C:\iis_www" -Port 8080
```

### Création de la liaison

Afin de créer la liaison du nouveau site vers le port 8080, il faut tout d'abord se logguer en tant qu'administrateur local.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer la liaison du nouveau site vers le port 8080.

```Powershell
New-WebBinding -Name "MON Site IIS" -IPAddress "*" -Port 8080
```

### Création de la liaison vers le document par défaut

Afin de créer la liaison du nouveau site IIS vers le document par défaut iis.html, il faut tout d'abord se logguer en tant qu'administrateur local.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer la liaison du nouveau site IIS vers le document par défaut.

```
Add-WebConfiguration system.webserver/defaultdocument/files
"IIS:\sites\MON Site IIS" -Value iis.html
```

\newpage

# PHP

## Installation du Visual C++ Visual Studio 2017

Afin d'installer Visual C++ VC 2017, il faut tout d'abord se logguer en tant qu'administrateur local.

Il faut tout d'abord télécharger le fichier d'installation sur le site de Microsoft.

Puis, il faut double-cliquer dessus pour lancer l'installateur.

Il faut ensuite cocher la case à cocher nommée "I agree to the license terms and conditions" et cliquer sur le bouton Install.

## Installation du module PHP 7.2

Afin d'installer le module PHP pour IIS, il faut tout d'abord se logguer en tant qu'administrateur local.

Il faut tout d'abord télécharger le module au format .zip dans la version Non-Thread-Safe.

Puis, il faut décompresser le fichier .zip dans le dossier C:\\php en cliquant-droit sur le fichier et en sélectionnant Extract All.

Ensuite, il faut saisir l'emplacement désiré dans le champ "Files will be extracted to this folder" et cliquer sur le bouton Extract.

### Configuration du php.ini

Puis, il faut renommer le fichier php.ini-production à php.ini en cliquant-droit sur le fichier et en sélectionnant Rename.

Après cela, il faut modifier le contenu du fichier php.ini en cliquant-droit dessus et en sélectionnant Edit.

Puis, il faut modifier les lignes suivantes.

```ini
fastcgi.impersonate = 1
cgi.fix_pathinfo = 0
cgi.force_redirect = 0
open_basedir = "C:\iis_www"
extension_dir = "./ext"
error_log="./php_errors.log"
```

Il faut ensuite cliquer sur le menu File puis sélectionner Save.

### Ajout du dossier dans le PATH

Puis, il faut ajouter le dossier dans lequel est situé php dans le PATH système.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter C:\\php à la variable PATH système.

```Powershell
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";C:\php",
 [EnvironmentVariableTarget]::Machine)
```

### Ajout du script handler

Puis, il faut ajouter un mapping handler pour les fichiers .php.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter le script handler pour les fichiers .php.

```Powershell
New-WebHandler -Name "FastCGI" -Path "*.php"  -Modules "FastCgiModule"
-ScriptProcessor "C:\php\php-cgi.exe" -PSPath "IIS:\sites\MON Site IIS"
-Verb "*"
```

### Ajout du document par défaut index.php

Puis, il faut ajouter le document par défaut index.php au site IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter le document par défaut index.php au site IIS.

```Powershell
Add-WebConfiguration system.webserver/defaultdocument/files
"IIS:\sites\MON Site IIS" -Value index.php
```

### Ajout du document par défaut default.php

Puis, il faut ajouter le document par défaut default.php au site IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter le document par défaut default.php au site IIS.

```Powershell
Add-WebConfiguration system.webserver/defaultdocument/files
"IIS:\sites\MON Site IIS" -Value default.php
```

### Redémarrage d'IIS

Puis, il faut redémarrer le service IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de redémarrer le service IIS.

```Powershell
iisreset /noforce
```

## Test du module PHP

### Création du fichier php

Afin de pouvoir tester si PHP fonctionne sous IIS, il faut créer un fichier nommé index.php placé dans C:\\iis_www en cliquant-droit et en sélectionnant New puis Text File.

Il faut ensuite saisir les lignes suivantes dans le fichier index.php.

```php
<?php phpinfo(); ?>
```

Il faut ensuite cliquer sur le menu File puis sélectionner Save.

### Visualiation de index.php

Il faut ensuite ouvrir un navigateur web et saisir http://IPIIS:8080/index.php.

La page d'information de PHP devrait s'afficher.

\newpage

# Authentification

## Création du serveur Active Directory

### Création de la machine virtuelle

Afin de pouvoir installer le serveur faisant office de contrôleur de domaine Active Directory, il est nécessaire de créer une machine virtuelle.

### Installation de Windows Server 2016

Afin de pouvoir installer le rôle Active Directory, il est tout d'abord nécessaire d'installer le système d'exploitation Windows Server 2016.

Puis, après l'installation, il faut se connecter à l'aide du compte administrateur local et du mot de passe défini lors de l'installation.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Ensuite, il faut saisir la commande suivante pour définir l'adresse IP du serveur.

```Powershell
netsh interface ip set address "Ethernet0"
static 192.168.55.5 255.255.255.0 192.168.55.1
```

Puis, il faut saisir la commande suivante pour définir l'addresse IP du serveur DNS.

```Powershell
netsh interface ip set dns "Ethernet0" static 127.0.0.1
```

Ensuite, il faut saisir la commande suivante afin de définir le nom d'hôte du serveur.

```Powershell
Rename-Computer -NewName AD -PassThru -Force
```

Après cela, il faut saisir les commandes suivantes afin de définir le serveur de temps.

```Powershell
Stop-Service w32Time
w32tm /config /manualpeerlist:0.ch.pool.ntp.org /syncfromflags:MANUAL
/reliable:yes /update
Restart-Service w32Time
w32tm /resync
```

Finalement, il faut saisir la commande suivante pour redémarrer le serveur.

```Powershell
Restart-Computer
```

### Installation du rôle Active Directory

Afin de pouvoir utiliser l'authentification Windows, il est tout d'abord nécessaire d'installer le rôle AD DS sur le deuxième serveur.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'installer le rôle Active Directory Domain Services.

```Powershell
Import-Module ServerManager
Install-WindowsFeature AD-Domain-Services
Add-WindowsFeature RSAT-AD-PowerShell,RSAT-AD-AdminCenter,RSAT-ADDS-Tools
```

### Promotion en tant que contrôleur de domaine

Afin de pouvoir utiliser l'authentification Windows, il est tout d'abord nécessaire de promouvoir en contrôleur de domaine le deuxième serveur.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'effectuer la promotion du serveur.

```Powershell
Import-Module ADDSDeployment
Install-ADDSForest -CreateDnsDelegation:$false -DatabasePath "D:\NTDS"
-DomainMode "Default" -DomainName "team11.local" -DomainNetbiosName "TEAM11"
-ForestMode "Default" -InstallDns:$true -LogPath "D:\NTDS"
-NoRebootOnCompletion:$false -SysvolPath "D:\SYSVOL" -Force:$true
```

### Création des utilisateurs

## Ajout du serveur IIS dans le domaine

Afin de pouvoir utiliser l'authentification Windows, il est nécessaire d'ajouter le serveur IIS dans le domaine Active Directory.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter le serveur contrôleur de domaine en tant que serveur de temps.

```Powershell
Stop-Service w32Time
w32tm /config /manualpeerlist:192.168.55.5 /syncfromflags:MANUAL
/reliable:yes /update
Restart-Service w32Time
w32tm /resync
```

Il faut ensuite saisir la commande suivante afin d'ajouter le serveur IIS dans le domaine Active Directory.

```Powershell
Add-Computer -NewName IIS -DomainName team11.local
-Credential TEAM11\Administrator -Restart -Force
```

## Définition du mode d'authentification

Afin de pouvoir utiliser l'authentification Windows, il est nécessaire de définir le mode d'authentification du site IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de définir le mode d'authentification Windows sur le site IIS.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/windowsAuthentication"
-Name Enabled -Value True -PSPath "IIS:\sites\MON Site IIS"
```

Il faut ensuite saisir la commande suivante afin de désactiver le mode d'authentification anonyme sur le site IIS.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/anonymousAuthentication"
-Name Enabled -Value False -PSPath "IIS:\sites\MON Site IIS"
```

Il faut ensuite saisir la commande suivante afin de redémarrer le service IIS.

```Powershell
iisreset /noforce
```

\newpage

# FTP

## Installation du rôle FTP Server

Afin de pouvoir utiliser un serveur FTP, il est tout d'abord nécessaire d'installer le rôle sur le serveur IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'installer le rôle FTP.

```Powershell
Install-WindowsFeature Web-Ftp-Server,Web-Ftp-Service,Web-Ftp-Ext
```

## Configuration du rôle FTP

Afin de pouvoir utiliser un serveur FTP, il est tout d'abord nécessaire de configurer le site FTP.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de configurer le service FTP.

```Powershell
New-WebFtpSite -Name "FTP" -Port 21 -PhysicalPath "c:\iis_www"
```

## Test du serveur FTP

Afin de pouvoir utiliser un serveur FTP, il est tout d'abord nécessaire de s'y connecter à l'aide d'un client FTP.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de se connecter au service FTP.

```Powershell
ftp
open 127.0.0.1
```

\newpage

# Sauvegarde

## Installation du rôle de sauvegarde

Afin de pouvoir effectuer des sauvegardes, il est tout d'abord nécessaire d'installer le rôle de sauvegarde sur le serveur.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'installer le rôle de sauvegarde.

```Powershell
Install-WindowsFeature Windows-Server-Backup
```

## Configuration de la sauvegarde de base

Afin de pouvoir effectuer des sauvegardes, il est tout d'abord nécessaire de configurer le rôle de sauvegarde sur le serveur.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de configurer la sauvegarde de l'état du système.

```Powershell
$BackupPolicy = New-WBPolicy
Add-WBBareMetalRecovery $BackupPolicy
$BackupLocation = New-WBBackupTarget -VolumePath D:
Add-WBBackupTarget -Policy $BackupPolicy -Target $BackupLocation
Set-WBSchedule -Policy $BackupPolicy -Schedule 00:00
Set-WBVssBackupOptions -Policy $BackupPolicy -VssCopyBackup
Set-WBPolicy -Force -Policy $BackupPolicy
```

## Sauvegarde du contenu des sites IIS

Afin de pouvoir effectuer des sauvegardes, il est tout d'abord nécessaire de configurer les paramètres de la sauvegarde des sites IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de configurer la sauvegarde des sites IIS.

```Powershell
$BackupIISSitePolicy = New-WBPolicy
$FolderToBackupPath = New-WBFileSpec -FileSpec "C:\iis_www"
Add-WBFileSpec -Policy $BackupIISSitePolicy -FileSpec $FolderToBackupPath
$BackupLocation = New-WBBackupTarget -VolumePath D:
Add-WBBackupTarget -Policy $BackupIISSitePolicy -Target $BackupLocation
Set-WBSchedule -Policy $BackupIISSitePolicy -Schedule 00:00
Set-WBVssBackupOptions -Policy $BackupIISSitePolicy -VssCopyBackup
Set-WBPolicy -Force -Policy $BackupIISSitePolicy
```

Puis, il faut saisir la commande suivante afin de démarrer la sauvegarde.

```
Start-WBBackup -Policy $BackupIISSitePolicy
```

## Exportation de la configuration IIS

### Sauvegarde de la configuration

Afin de pouvoir effectuer des sauvegardes de la configuration d'IIS, il est tout d'abord nécessaire de créer des backup à l'aide de l'outil appcmd.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer une sauvegarde de la configuration d'IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe add backup "Backup"
```

### Exportation des Application Pool

Si l'on a configuré des Application Pools supplémentaires, il est tout d'abord nécessaire de les exporter à l'aide de l'outil appcmd.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'exporter les Applications Pools.

```Powershell
C:\Windows\system32\inetsrv\appcmd list apppool /config /xml
> C:\Windows\system32\inetsrv\apppools.xml
```

### Exportation de la configuration des sites IIS

Si l'on a configuré des sites IIS supplémentaires, il est tout d'abord nécessaire de les exporter à l'aide de l'outil appcmd.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'exporter la configuration des sites IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd list site /config /xml
> C:\Windows\system32\inetsrv\sites.xml
```

## Sauvegarde de la configuration IIS

Afin de pouvoir effectuer des sauvegardes, il est tout d'abord nécessaire de configurer les paramètres de la configuration IIS.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de configurer la sauvegarde de la configuration de IIS.

```Powershell
$BackupIISConfigurationPolicy = New-WBPolicy
$Filespec = New-WBFileSpec -FileSpec "C:\windows\system32\inetsrv\"
Add-WBFileSpec -Policy $BackupIISConfigurationPolicy -FileSpec $FileSpec
$BackupLocation = New-WBBackupTarget -VolumePath D:
Add-WBBackupTarget -Policy $BackupIISConfigurationPolicy -Target $BackupLocation
Set-WBSchedule -Policy $BackupIISConfigurationPolicy -Schedule 00:00
Set-WBVssBackupOptions -Policy $BackupIISConfigurationPolicy -VssCopyBackup
Set-WBPolicy -Force -Policy $BackupIISConfigurationPolicy
```

Puis, il faut saisir la commande suivante afin de démarrer la sauvegarde.

```
Start-WBBackup -Policy $BackupIISConfigurationPolicy
```

## Restauration de la sauvegarde

Afin de restaurer des sauvegardes, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de restaurer le dossier du contenu des sites IIS.

```Powershell
$Backup = Get-WBBackupSet
Start-WBFileRecovery -BackupSet $Backup
-FilePathToRecover "C:\iis_www" -Recursive
-FileRecoveryOption CreateCopyIfExists -Force
```

Si l'on désire restaurer la configuration des sites IIS, il faut saisir la commande suivante.

```Powershell
$Backup = Get-WBBackupSet
Start-WBFileRecovery -BackupSet $Backup
-FilePathToRecover "C:\windows\system32\inetsrv\" -Recursive
-FileRecoveryOption CreateCopyIfExists -Force
```

## Restauration de la configuration IIS

### Importation des Application Pool

Si l'on a configuré des Application Pools supplémentaires, il est tout d'abord nécessaire de les importer à l'aide de l'outil appcmd.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'importer les Applications Pools.

```Powershell
C:\Windows\system32\inetsrv\appcmd add apppool /in
< C:\Windows\system32\inetsrv\apppools.xml
```

### Importation des sites IIS

Si l'on a configuré des sites IIS supplémentaires, il est tout d'abord nécessaire de les importer à l'aide de l'outil appcmd.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'importer la configuration des sites IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd add site /in
< C:\Windows\system32\inetsrv\sites.xml
```

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://fr.wikipedia.org/w/index.php?title=Internet_Information_Services&oldid=140016257, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/install/installing-iis-85/installing-iis-85-on-windows-server-2012-r2, consulté le 30.01.2018.

http://www.tomsitpro.com/articles/powershell-manage-iis-websites,2-994.html, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/application-frameworks/install-and-configure-php-on-iis/install-and-configure-php, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/powershell/module/webadminstration/set-webhandler?view=winserver2012-ps&viewFallbackFrom=winserver2012r2-ps, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/powershell/module/webadministration/new-webftpsite?view=win10-ps, consulté le 30.01.2018.

https://blogs.iis.net/bills/how-to-backup-restore-iis7-configuration, consulté le 30.01.2018.

\newpage

# Annexes

Ment.
