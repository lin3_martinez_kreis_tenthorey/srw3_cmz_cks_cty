---
title: SRW3 - Documentation LAMP Partie 2
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\lamp.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction
