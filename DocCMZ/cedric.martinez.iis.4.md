---
title: SRW3 - Documentation IIS Partie 4
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\iis.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

## Prérequis

Afin de pouvoir suivre le guide d'installation, il est nécessaire de disposer d'un hyperviseur ainsi que d'une image ISO de Windows Server 2016. Le guide sera effectué en utilisant l^hyperviseur VMware Workstation. La machine virtuelle devra avoir été créee au préalable en utilisant les paramètres par défaut.

\newpage

# Mise en place de l'infrastructure

## Configuration des cartes réseaux

Afin de pouvoir être acvcessible depuis le LAN et le WAN, il est nécessaire d'avoir deux cartes réseaux sur la VM. Pour ce faire, il faut ouvrir VMware Workstation et cliquer sur la VM.

Puis, il faut cliquer sur le menu VM et cliquer sur le bouton Settings.

Dans la fenêtre qui s'ouvre, il faut cliquer sur le bouton Add, cliquer sur Network Adapter sur le bouton Next.

Après cela, il faut cliquer sur le radio-bouton Bridged et sur le bouton Finish.

## Configuration réseau

Il faut ensuite se connecter avec un compte administrateur du domaine sur le serveur IIS.

Puis, il faut presser sur les touches Windows et R simultanément.

Après cela, il faut saisir powershell et appuyer sur la touche Enter.

Puis, il faut saisir la commande suivant afin de définir l'adresse IP de l'interface WAN.

```Powershell
Set-NetIPAddress -InterfaceAlias Ethernet1
-IPAddress 172.17.217.130 -PrefixLength 16
```

Ensuite, il faut saisir la commande suivante afin de définir le serveur DNS de l'interface WAN.

```Powershell
Set-DnsClientServerAddress -InterfaceAlias Ethernet1
-ServerAddresses ("172.17.10.2","172.17.10.20")
```

## Création des utilisateurs

Afin de pouvoir authentifier les utilisateurs, il faut tout d'abord les créer dans l'Active Directory.

Pour ce faire, il faut se connecter au serveur Active Directory avec un nom d'utilisateur administrateur du domaine.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer les comptes utilisateurs en remplacant USER par le nom d'utilisateur, NOM par le nom de l'utilisateur, PRENOm par le prénom de l'utilisateur et presser sur la touche Enter.

```Powershell
New-ADUser -Name "USER" -GivenName PRENOM -Surname NOM -SamAccountName USER
-UserPrincipalName USER@team11.local
-AccountPassword (Read-Host -AsSecureString "AccountPassword") -PassThru | Enable-ADAccount
```

Puis, il faut saisir le mot de passe de l'utilisateur et presser sur la touche Enter.

Il faut répéter cette action pour tous les utilisateurs qui doivent être créés.

## Création des groupes

Afin de pouvoir authentifier les utilisateurs selon leur groupe, il faut tout d'abord les créer dans l'Active Directory.

Pour ce faire, il faut se connecter au serveur Active Directory avec un nom d'utilisateur administrateur du domaine.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le groupe Management.

```Powershell
New-ADGroup -Name "Grp_Management" -SamAccountName Grp_Management
-GroupCategory Security -GroupScope Global -DisplayName "Management"
-Path "CN=Users,DC=Team11,DC=local"
-Description "Members of this group are Managers"
```

Il faut ensuite saisir la commande suivante afin de créer le groupe Engineering.

```Powershell
New-ADGroup -Name "Grp_Engineering" -SamAccountName Grp_Engineering
-GroupCategory Security -GroupScope Global -DisplayName "Engineering"
-Path "CN=Users,DC=Team11,DC=local"
-Description "Members of this group are Engineers"
```

Il faut ensuite saisir la commande suivante afin de créer le groupe Accounting.

```Powershell
New-ADGroup -Name "Grp_Accounting" -SamAccountName Grp_Accounting
-GroupCategory Security -GroupScope Global -DisplayName "Accounting"
-Path "CN=Users,DC=Team11,DC=local" -Description "Members of this group are Accountants"
```

Il faut ensuite saisir la commande suivante afin de créer le groupe Customers.

```Powershell
New-ADGroup -Name "Grp_Customers" -SamAccountName Grp_Customers
-GroupCategory Security -GroupScope Global -DisplayName "Customers"
-Path "CN=Users,DC=Team11,DC=local" -Description "Members of this group are Customers"
```

## Ajout des utilisateurs dans leur groupe

Afin de pouvoir authentifier les utilisateurs selon leur groupe, il faut tout d'abord ajouter les utilisateurs dans les groupes dans l'Active Directory.

Pour ce faire, il faut se connecter au serveur Active Directory avec un nom d'utilisateur administrateur du domaine.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'ajouter l'utilisateur mdupont au groupe Management.

```Powershell
Add-ADGroupMember Grp_Management mdupont
```

Il faut ensuite saisir la commande suivante afin d'ajouter les utilisateurs ingénieurs au groupe Engineering.

```Powershell
Add-ADGroupMember Grp_Engineering jbricot,jdeuf,kdiocy
```

Il faut ensuite saisir la commande suivante afin d'ajouter l'utilisateur comptable au groupe Accounting.

```Powershell
Add-ADGroupMember Grp_Accounting massain
```

Il faut ensuite saisir la commande suivante afin d'ajouter l'utilisateur énérique client au groupe Customers.

```Powershell
Add-ADGroupMember Grp_Customers dclient
```

# Mise en place d'un intranet et d'un extranet

## Création des dossiers

Afin de pouvoir créer les sites, il est nécessaire de créer les dossiers qui hebergeront les sites.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le dossier du TP.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www
```

Il faut ensuite saisir la commande suivante afin de créer le dossier du site Internet accessible depuis le WAN.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www/internet
```

Il faut ensuite saisir la commande suivante afin de créer le dossier des clients.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www/internet/clients
```

Il faut ensuite saisir la commande suivante afin de créer le dossier du site Intranet.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www/intranet
```

Il faut ensuite saisir la commande suivante afin de créer le dossier de la section private.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www/intranet/private
```

Il faut ensuite saisir la commande suivante afin de créer le dossier des utilisateurs.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www/intranet/private/users
```

Il faut ensuite saisir la commande suivante afin de créer le dossier de l'utilisateur en remplacant USER par le nom d'utilisateur.

```Powershell
New-Item -ItemType directory -Path C:/SRW/www/intranet/private/users/USER
```

Il faut répéter la dernière commande avec tout les utilisateurs désirés.

## Création des fichiers

Afin de pouvoir créer les sites, il est nécessaire de créer les fichiers que hebergeront les sites.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier index.html du site Internet.

```Powershell
New-Item -ItemType File -Path C:/SRW/www/internet/index.html
```

Il faut ensuite saisir la commande suivante afin de créer le fichier d'acceuil des clients.

```Powershell
New-Item -ItemType File -Path C:/SRW/www/internet/clients/infoclient.html
```

Il faut ensuite saisir la commande suivante afin de créer le fichier index.html du site Intranet.

```Powershell
New-Item -ItemType File -Path C:/SRW/www/intranet/index.html
```

Il faut ensuite saisir la commande suivante afin de créer le fichier de l'utilisateur en remplacant USER par le nom d'utilisateur.

```Powershell
New-Item -ItemType File -Path C:/SRW/www/intranet/private/users/USER/index.html
```

Il faut répéter la dernière commande avec tout les utilisateurs désirés.

## Modification des permissions NTFS

Afin de modifier les permissions des dossiers des utilisateurs, il faut tout d'abord télécharger les cmdlets pour Powershell File System Security PowerShell Module 4.2.3 disponibles à l'adresse : https://gallery.technet.microsoft.com/scriptcenter/1abd77a5-9c0b-4a2b-acef-90dbb2b84e85

Ensuite, il faut extraire le fichier .zip dans le dossier C:\\Users\\USER\\Documents\\WindowsPowerShell\\Modules

Il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Puis, il faut saisir la commande suivante afin d'autoriser l'exécution des cmdlets.

```Powershell
Set-ExecutionPolicy Unrestricted
```

Puis, il faut saisir la commande suivante afin d'importer le cmdlet.

```Powershell
Import-Module NTFSSecurity
```

Puis, il faut saisir la commande suivante afin de donner les droits totaux aux administrateurs du domaine.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/intranet/private/users/
-Account 'DOMAIN\Domain Admins' -AccessRights FullControl
```

Puis, il faut saisir la commande suivante afin de donner les droits de lecture aux membres du groupe IIS qui permet au serveur de lire le fichier web.config.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/intranet/private/users/
-Account 'DOMAIN\IIS_IUSRS' -AccessRights Read
```

Puis, il faut saisir la commande suivante afin de désactiver l'héritage.

```Powershell
Disable-Inheritence C:/SRW/www/intranet/private/users
```

Puis, il faut saisir la commande suivante en remplacant USER par le nom d'utilisateur désiré afin d'autoriser l'accès à cet utilisateur.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/intranet/private/users/USER
-Account 'DOMAIN\USER' -AccessRights Modify
```

Puis, il faut saisir la commande suivante en remplacant USER par le nom d'utilisateur désiré afin d'interdire l'accès aux autres utilisateurs.

```Powershell
Remove-NTFSAccess -Path C:/SRW/www/intranet/private/users/USER
-Account 'HOSTNAME\Users'
```

Puis, il faut saisir la commande suivante afin de donner les droits de lecture aux membres du groupe IIS qui permet au serveur de lire le fichier web.config.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/intranet/private/users/USER
-Account 'DOMAIN\IIS_IUSRS' -AccessRights Read
```

Il faut répéter les trois dernières commandes avec tout les utilisateurs désirés.

## Création du site Intranet

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer le site IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le site IIS labo du Module SRW.

```Powershell
C:\Windows\system32\inetsrv\appcmd add site /name:"Intranet"
/physicalPath:"C:\SRW\www\Intranet" /bindings:http/192.168.55.10:80:
```

Il faut ensuite saisir la commande suivante afin de créer le Pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd add apppool /name:"Intranet"
```

Il faut ensuite saisir la commande suivante afin de définir le pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd set app "Intranet/" /applicationPool:"Intranet"
```

Il faut ensuite saisir la commande suivante afin de définir le dossier virtuel du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd add vdir /app.name:"Intranet/"
 /path:/internet /physicalPath:C:\SRW\www\Internet
```

Il faut ensuite saisir la commande suivante afin de définir le mode d'authentification Windows sur le site IIS private et les sous-dossiers.

```Powershell
Set-WebConfigurationProperty
-Filter "/system.webServer/security/authentication/windowsAuthentication"
-Name Enabled -Value True -PSPath "IIS:\sites\Intranet/private"
```

Il faut ensuite saisir la commande suivante afin de désactiver le mode d'authentification anonyme sur le site IIS private et les sous-dossiers.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/anonymousAuthentication"
-Name Enabled -Value False -PSPath "IIS:\sites\Internet/private"
```

Il faut ensuite saisir la commande suivante afin de restrindre l'accès à l'utilisateur spécifique au site IIS users de l'utilisateur en remplacant USER par le nom d'utilisateur.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "Intranet/private/users/USER"
-section:system.webServer/security/authorization
/+"[accessType='Allow',users='TEAM11\USER']"
```

Il faut répéter la dernière commande avec tout les utilisateurs désirés.

Il faut ensuite saisir la commande suivante afin de restrindre l'accès à un groupe spécifique au site IIS utilisateur.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "Intranet/private/users/USER"
-section:system.webServer/security/authorization
/+"[accessType='Deny',users='HOSTNAME\Users']"
```

Il faut ensuite saisir la commande suivante afin de démarrer le site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd start site /site.name:"Intranet"
```

## Modification des permissions NTFS

Afin de modifier les permissions des dossiers des utilisateurs, il faut tout d'abord télécharger les cmdlets pour Powershell File System Security PowerShell Module 4.2.3 disponibles à l'adresse : https://gallery.technet.microsoft.com/scriptcenter/1abd77a5-9c0b-4a2b-acef-90dbb2b84e85

Ensuite, il faut extraire le fichier .zip dans le dossier C:\\Users\\USER\\Documents\\WindowsPowerShell\\Modules

Il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Puis, il faut saisir la commande suivante afin d'autoriser l'exécution des cmdlets.

```Powershell
Set-ExecutionPolicy Unrestricted
```

Puis, il faut saisir la commande suivante afin d'importer le cmdlet.

```Powershell
Import-Module NTFSSecurity
```

Puis, il faut saisir la commande suivante afin de donner les droits totaux aux administrateurs du domaine.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/internet/
-Account 'DOMAIN\Domain Admins' -AccessRights FullControl
```

Puis, il faut saisir la commande suivante afin de donner les droits de lecture aux membres du groupe IIS qui permet au serveur de lire le fichier web.config.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/internet/clients
-Account 'DOMAIN\IIS_IUSRS' -AccessRights Read
```

Puis, il faut saisir la commande suivante afin de désactiver l'héritage.

```Powershell
Disable-Inheritence C:/SRW/www/internet/clients
```

Puis, il faut saisir la commande suivante en remplacant DOMAIN par le domaine désiré afin d'autoriser l'accès au groupe des ingénieurs.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/internet/clients
-Account 'DOMAIN\Grp_Engineers' -AccessRights Modify
```

Puis, il faut saisir la commande suivante en remplacant DOMAIN par le domaine désiré afin d'autoriser l'accès au groupe des clients.

```Powershell
Add-NTFSAccess -Path C:/SRW/www/internet/clients
-Account 'DOMAIN\Grp_Customers' -AccessRights Read
```

Puis, il faut saisir la commande suivante afin d'interdire l'accès aux autres utilisateurs.

```Powershell
Remove-NTFSAccess -Path C:/SRW/www/internet/clients
-Account 'HOSTNAME\Users'
```

## Création du site Internet

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer le site IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le site IIS labo du Module SRW.

```Powershell
C:\Windows\system32\inetsrv\appcmd add site /name:"Internet"
/physicalPath:"C:\SRW\www\Internet" /bindings:http/172.17.217.130:80:
```

Il faut ensuite saisir la commande suivante afin de créer le Pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd add apppool /name:"Internet"
```

Il faut ensuite saisir la commande suivante afin de définir le pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd set app "Internet/"
/applicationPool:"Internet"
```

Il faut ensuite saisir la commande suivante afin de définir le dossier virtuel du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd add vdir /app.name:"Internet/"  
/path:/intranet /physicalPath:C:\SRW\www\Intranet
```

Il faut ensuite saisir la commande suivante afin de démarrer le site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd start site /site.name:"Internet"
```

Il faut ensuite saisir la commande suivante afin de définir le mode d'authentification de base sur le site IIS Intranet.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/baseAuthentication"
-Name Enabled -Value True -PSPath "IIS:\sites\Internet/intranet"
```

Il faut ensuite saisir la commande suivante afin de désactiver le mode d'authentification anonyme sur le site IIS Intranet.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/anonymousAuthentication"
-Name Enabled -Value False -PSPath "IIS:\sites\Internet/intranet"
```

Il faut ensuite saisir la commande suivante afin de définir le mode d'authentification de base sur le site IIS clients.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/baseAuthentication"
-Name Enabled -Value True -PSPath "IIS:\sites\Internet/clients"
```

Il faut ensuite saisir la commande suivante afin de désactiver le mode d'authentification anonyme sur le site IIS clients.

```Powershell
Set-WebConfigurationProperty -Filter
"/system.webServer/security/authentication/anonymousAuthentication"
-Name Enabled -Value False -PSPath "IIS:\sites\Internet/clients"
```

Il faut ensuite saisir la commande suivante afin de restrindre l'accès à un groupe spécifique au site IIS clients.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "Internet/clients"
-section:system.webServer/security/authorization
/+"[accessType='Allow',users='TEAM11\Grp_Customers']"
```

Il faut ensuite saisir la commande suivante afin de restrindre l'accès à un groupe spécifique au site IIS clients.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "Internet/clients"
-section:system.webServer/security/authorization
/+"[accessType='Allow',users='TEAM11\Grp_Engineering']"
```

Il faut ensuite saisir la commande suivante afin de restrindre l'accès à un groupe spécifique au site IIS clients.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe set config "Internet/clients"
-section:system.webServer/security/authorization
/+"[accessType='Deny',users='HOSTNAME\Users']"
```

## Ajout des ingénieurs en tant qu'administrateurs des sites IIS

Afin d'ajouter le groupe des ingénieurs aux personnes autorisées à administrer les sites Internet et Intranet, il faut modifer un fichier de configuration.

Puis, il faut modifier le fichier C:\\Windows\\system32\\inetsrv\\config\\administration.config et y ajouter les lignes suivantes dans la section system.webServer/security/authorization.

```xml
<authorizationRules>
    <scope path="/Internet">
        <add name="TEAM11\Grp_Engineering" isRole="true" />
    </scope>
    <scope path="/Intranet">
        <add name="TEAM11\Grp_Engineering" isRole="true" />
    </scope>
</authorizationRules>
```

Il faut ensuite sauvegarder le fichier.

\newpage

# Redirection HTTP vers HTTPS

## Génération du certificat

Afin de pouvoir faire des redirection de http vers https, il faut tout d'abord générer le certificat qui sera utilisé par le protocole https.

Pour ce faire, il faut se connecter sur le serveur avec des identifiants d'administrateur.

Puis, lancer l'outil Server Manager.

Après cela, il faut cliquer sur le menu Tools du Server Manager et sélectionner Internet Information Services (IIS) Manager.

Ensuite, il faut cliquer sur le nom du serveur IIS dans le menu de gauche.

Puis, il faut cliquer sur l'icône nommée Server Certificates.

Après, il faut cliquer sur le lien Create Self-Signed Certificate dans le menu de droite.

Dans la fenêtre qui s'ouvre, il faut ensuite saisir le nom désiré du certificat dans le champ Specify a friendly name for the certificate, sélectionner Web Hosting dans le champ Select a certificate store for the new certificate et cliquer sur le bouton OK.

## Activation de https

Afin de pouvoir faire des redirection de http vers https, il faut tout d'abord activer le protocole https.

Pour ce faire, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'activer le protocole https pour le site Internet.

```Powershell
C:\Windows\system32\inetsrv\appcmd set site /site.name:"Internet"
"/+bindings.[protocol='https',bindingInformation='172.17.217.130:443:']"
```

Il faut ensuite saisir la commande suivante afin d'activer le protocole https pour le site Intranet.

```Powershell
C:\Windows\system32\inetsrv\appcmd set site /site.name:"Intranet"
"/+bindings.[protocol='https',bindingInformation='192.168.55.10:443:']"
```

## Création d'une redirection http vers https

### Redirection http vers https sur le site Internet

Afin de pouvoir faire des redirection de http vers https, il faut tout d'abord installer le module http urlrewrite.

Pour ce faire, il faut télécharger le fichier à l'URL https://www.iis.net/downloads/microsoft/url-rewrite

Puis, il faut double-cliquer sur le fichier télécharger afin de lancer l'installation.

Ensuite, il faut ouvrir le fichier web.config situé dans le dossier C:\\SRW\\www\\Internet et saisir le code xml suivant.

```xml
<?xml version="1.0" encoding="UTF-8"?>
	<configuration>
    <location path="." inheritInChildApplications="false">
		<system.webServer>
			<rewrite>
				<rules>
					<rule name="Redirect to HTTPS" enabled="false" stopProcessing="true">
						<match url="(.*)" />
						<conditions><add input="{HTTPS}" pattern="^OFF$" />
						</conditions>
						<action type="Redirect" url="https://{HTTP_HOST}/{R:1}" redirectType="SeeOther" />
					</rule>
				</rules>
			</rewrite>
		</system.webServer>
	</configuration>
```


Puis, il faut ensuite sauvegarder le fichier.

### Redirection http vers https sur le site Intranet

Ensuite, il faut ouvrir le fichier web.config situé dans le dossier C:\\SRW\\www\\Intranet et saisir le code xml suivant.

```xml
<?xml version="1.0" encoding="UTF-8"?>
	<configuration>
    <location path="." inheritInChildApplications="false">
		<system.webServer>
			<rewrite>
				<rules>
					<rule name="Redirect to HTTPS" enabled="false" stopProcessing="true">
						<match url="(.*)" />
						<conditions><add input="{HTTPS}" pattern="^OFF$" />
						</conditions>
						<action type="Redirect" url="https://{HTTP_HOST}/{R:1}" redirectType="SeeOther" />
					</rule>
				</rules>
			</rewrite>
		</system.webServer>
	</configuration>
```

Puis, il faut ensuite sauvegarder le fichier.

### Activation de la redirection sur le site Internet

Afin de pouvoir faire des redirections de http vers https, il faut tout d'abord activer les redirections.

Pour ce faire, il faut se connecter sur le serveur avec des identifiants d'administrateur.

Puis, il faut lancer l'outil Server Manager.

Après cela, il faut cliquer sur le menu Tools du Server Manager et sélectionner Internet Information Services (IIS) Manager.

Ensuite, il faut cliquer sur le nom du serveur IIS dans le menu de gauche, puis sur le site Internet.

Puis, il faut cliquer sur l'icône nommée URL Rewrite.

Après, il faut cliquer sur le lien Enable Rule dans le menu de droite.

### Activation de la redirection sur le site Internet

Afin de pouvoir faire des redirections de http vers https, il faut tout d'abord activer les redirections.

Pour ce faire, il faut se connecter sur le serveur avec des identifiants d'administrateur.

Puis, il faut lancer l'outil Server Manager.

Après cela, il faut cliquer sur le menu Tools du Server Manager et sélectionner Internet Information Services (IIS) Manager.

Ensuite, il faut cliquer sur le nom du serveur IIS dans le menu de gauche, puis sur le site Intranet dans le menu de gauche.

Puis, il faut cliquer sur l'icône nommée URL Rewrite.

Finalement, il faut cliquer sur le lien Enable Rule dans le menu de droite.

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://fr.wikipedia.org/w/index.php?title=Internet_Information_Services&oldid=140016257, consulté le 30 janvier 2018.

https://docs.microsoft.com/en-us/iis/install/installing-iis-85/installing-iis-85-on-windows-server-2012-r2, consulté le 30 janvier 2018.

http://www.tomsitpro.com/articles/powershell-manage-iis-websites,2-994.html, consulté le 30 janvier 2018.

https://docs.microsoft.com/en-us/iis/application-frameworks/install-and-configure-php-on-iis/install-and-configure-php, consulté le 30 janvier 2018.

https://docs.microsoft.com/en-us/powershell/module/webadminstration/set-webhandler?view=winserver2012-ps&viewFallbackFrom=winserver2012r2-ps, consulté le 30 janvier 2018.

https://docs.microsoft.com/en-us/powershell/module/webadministration/new-webftpsite?view=win10-ps, consulté le 30 janvier 2018.

https://blogs.iis.net/bills/how-to-backup-restore-iis7-configuration, consulté le 30 janvier 2018.

https://docs.microsoft.com/en-us/iis/get-started/getting-started-with-iis/create-a-web-site, consulté le 2 février 2018

https://docs.microsoft.com/en-us/iis/configuration/system.webserver/security/ipsecurity/add, consulté le 13 février 2018

http://techgenix.com/creating-active-directory-accounts-using-powershell/, consulté le 13 février 2018

https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc770966(v=ws.10), consulté le 13 février 2018

https://docs.microsoft.com/en-us/powershell/module/dnsclient/set-dnsclientserveraddress?view=win10-ps, consulté le 27 février 2018

https://docs.microsoft.com/en-us/powershell/module/activedirectory/new-adgroup?view=winserver2012-ps, consulté le 27 février 2018

https://www.digicert.com/csr-creation-ssl-installation-iis-10.htm, consulté le 5 mars 2018

https://docs.microsoft.com/en-us/iis/configuration/system.webserver/httpredirect/, consulté le 5 mars 2018

https://docs.microsoft.com/en-us/iis/configuration/system.applicationhost/sites/site/bindings/binding, consulté le 5 mars 2018

https://docs.microsoft.com/en-us/iis/manage/remote-administration/remote-administration-for-iis-manager, consulté le 5 mars 2018

https://www.iis.net/downloads/microsoft/url-rewrite, consulté le 5 mars 2018

https://gridscale.io/en/community/tutorials/iis-redirect-http-to-https-windows/, consulté le 5 mars 2018

\newpage

# Annexes

Ment.
