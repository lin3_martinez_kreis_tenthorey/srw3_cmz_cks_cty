---
title: SRW3 - Documentation IIS Partie 2
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\iis.png)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

## Prérequis

Afin de pouvoir suivre le guide d'installation, il est nécessaire de disposer d'un hyperviseur ainsi que d'une image ISO de Windows Server 2016. Le guide sera effectué en utilisant l^hyperviseur VMware Workstation. La machine virtuelle devra avoir été créée au préalable en utilisant les paramètres par défaut.

\newpage

# Exercice 1

## Création des répertoires

Afin de pouvoir créer les sites IIS, il faut tout d'abord créer les dossiers dans lesquels ils se situeront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le dossier du site IIS Site01.

```Powershell
New-Item -ItemType directory -Path C:/SRW/Site01
```

Afin de pouvoir créer les sites IIS, il faut tout d'abord créer les dossiers dans lesquels ils se situeront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le dossier du site IIS Repvirtuel.

```Powershell
New-Item -ItemType directory -Path C:/SRW/repvirtuel
```

## Création des fichiers html

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer les fichiers html qu'ils contiendront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier indexsite01.html du site Site01.

```Powershell
New-Item -ItemType File -Path C:/SRW/Site01/indexsite01.html
```

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer les fichiers html qu'ils contiendront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier indexvirtuel.html du site Repvirtuel.

```Powershell
New-Item -ItemType File -Path C:/SRW/repvirtuel/indexvirtuel.html
```

## Création du site

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer le site IIS.

Puis, il faut cliquer sur le menu Tools du Server Manager et sélectionner Internet Information Services (IIS) Manager.

Ensuite, il faut cliquer sur le nom du serveur IIS dans le menu de gauche.

Puis, il faut cliquer-droit sur Sites et sélectionner Add Website.

Ensuite, il faut saisir  Site01 du Module SRW dans le champ Site name et cliquer sur le bouton Browse.

Puis, il faut sélectionner le dossier C:\\SRV\\Site01 et cliquer sur le bouton OK.

Après cela, il faut saisir 8080 dans le champ Port et cliquer sur le bouton OK.

Puis, il faut cliquer-droit sur le nom du site, sélectionner Manage Website et cliquer sur Avanced Settings.

Ensuite, il faut saisir 31 dans le Champ ID et cliquer sur le bouton OK.

Puis, il faut cliquer sur le site, puis sur l'icône Default Document.

Après cela, il faut cliquer sur le lien Add du menu de droite.

Il faut ensuite saisir indexsite01.html dans le champ Name et cliquer sur le bouton OK.

Puis, il faut cliquer sur les autres pages et cliquer sur le lien Remove.

Après cela, il faut cliquer sur le site, puis sur l'icône Directory Browsing.

Ensuite, il faut cliquer sur le bouton Enable du menu de droite.

Après cela, il faut cliquer-droit sur le nom du site et sélectionner Add virtual directory.

Il faut ensuite saisir test1/ dans le champ Alias et C:\\SRV\\repvirtuel dans le champ Physical Path et cliquer sur le bouton OK.

## Installation du rôle Windows Search Service

```Powershell
Install-WindowsFeature Search-Service
```

## Configuration du catalogue de fichiers

Afin de pouvoir indexer les fichiers, il faut avoir créé un catalogue de fichiers.

Puis, il faut presser sur les touches Windows et R simultanément, saisir mmc et presser sur la touche Enter.

Ensuite, il faut cliquer sur l'icone Indexing Options.

Puis, il faut cliquer sur le bouton Modify.

Après cela, il faut sélectionner le dossier C:\\SRW et cliquer sur le bouton OK.

\newpage

# Exercice 2

## Création des répertoires

Afin de pouvoir créer les sites IIS, il faut tout d'abord créer les dossiers dans lesquels ils se situeront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le dossier du site IIS Site02.

```Powershell
New-Item -ItemType directory -Path C:/SRW/Site02
```

## Création des fichiers html

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer les fichiers html qu'ils contiendront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier indexsite02.html du site Site02.

```Powershell
New-Item -ItemType File -Path C:/SRW/Site02/indexsite02.html
```

## Création du site

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer le site IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd add site /name:"Site02 du Module SRW" /id:32 /physicalPath:"C:\SRW\Site02" /bindings:http/*/:8888
```

Il faut ensuite saisir la commande suivante afin de créer le Pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd add apppool /name:"Site02 du Module SRW"
```

Il faut ensuite saisir la commande suivante afin de définir le pool d'applications du site IIS Site02.

```Powershell
C:\Windows\system32\inetsrv\appcmd set app "Site02 du Module SRW/" /applicationPool:"Site02 du Module SRW"
```

Il faut ensuite saisir la commande suivante afin de définir le document par défaut du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd set config "Site02 du Module SRW" /section:defaultDocument "/+files.[@start,value='indexsite02.html']"
```

Il faut ensuite saisir la commande suivante afin d'activer le listing des dossiers.

```Powershell
C:\Windows\system32\inetsrv\appcmd set config "Site02 du Module SRW" /section:directoryBrowse /enabled:True
```

Il faut ensuite saisir la commande suivante afin de définir le dossier virtuel du site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd add vdir /app.name:"Site02 du Module SRW"  /path:/test2 /physicalPath:C:\SRW\repvirtuel
```

Il faut ensuite saisir la commande suivante afin de démarrer le site IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd start site /site.name:"Site02 du Module SRW"
```

\newpage

# Exercice 3

## Création des répertoires

Afin de pouvoir créer les sites IIS, il faut tout d'abord créer les dossiers dans lesquels ils se situeront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le dossier du site IIS Site03.

```Powershell
New-Item -ItemType directory -Path C:/SRW/Site03
```

## Création des fichiers html

Afin de pouvoir accéder aux sites IIS, il faut tout d'abord créer les fichiers html qu'ils contiendront.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier indexsite03.html du site Site03.

```Powershell
New-Item -ItemType File -Path C:/SRW/Site03/indexsite03.html
```

## Création du site

Afin de pouvoir accéder au site IIS, il faut tout d'abord modifier le fichier de configuration IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de modifier le fichier de configuration IIS.

```Powershell
notepad C:/Windows/system32/inetsrv/config/applicationHost.config
```
Ensuite, il faut y ajouter le code xml suivant en dessous de applicationsPools.

```xml
<add name="Site03 du Module SRW" />
```

Puis, le code suivant en dessous de sites.

```xml
<site name="Site03 du Module SRW" id="33">
    <application path="/" applicationPool="Site03 du Module SRW">
        <virtualDirectory path="/" physicalPath="D:\SRW\Site03" />
        <virtualDirectory path="/test3" physicalPath="D:\SRW\repvirtuel" />
    </application>
    <bindings>
        <binding protocol="http" bindingInformation="*:8880:" />
    </bindings>
</site>
```

Finalement, il faut sauvegarder le fichier en cliquant sur le menu File et en sélectionnant Save.

Afin de pouvoir accéder au site IIS, il faut créer le fichier de configuration du site IIS.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de créer le fichier du site IIS.

```Powershell
New-Item -ItemType File -Path C:/SRW/Site03/web.config
```

Puis, il faut saisir la commande suivante pour ouvrir le fichier de configuration.

```Powershell
notepad C:/SRW/Site03/web.config
```

Ensuite, il faut y ajouter le code xml suivant.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <system.webServer>
        <defaultDocument>
            <files>
                <add value="indexsite03.html" />
            </files>
        </defaultDocument>
        <directoryBrowse enabled="true" />
    </system.webServer>
</configuration>
```

Finalement, il faut sauvegarder le fichier en cliquant sur le menu File et en sélectionnant Save.

\newpage

# Appcmd

## Affichage des sites sur le serveur

Afin d'afficher les sites du serveur IIS il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'afficher la liste des sites IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd list site
```

## Affichage des sites écoutant sur 8080

Afin d'afficher les sites du serveur IIS qui écoutent sur le port 8080 il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'afficher la liste des sites IIS qui écoutent sur le port 8080.

```Powershell
C:\Windows\system32\inetsrv\appcmd list site /bindings:http/*:8080:
```

## Arrêt du site

Afin d'arrêter les sites IIS qui écoutent sur le port 8080 du serveur IIS il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin d'arrêter les sites IIS qui écoutent sur le port 8080.

```Powershell
C:\Windows\system32\inetsrv\appcmd list site /bindings:http/*:8080: /xml | C:\Windows\system32\inetsrv\appcmd stop site /in
```

## Démarrage du site

Afin de démarrer les sites IIS qui écoutent sur le port 8080 du serveur IIS il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de démarrer les sites IIS qui écoutent sur le port 8080.

```Powershell
C:\Windows\system32\inetsrv\appcmd list site /bindings:http/*:8080: /xml | C:\Windows\system32\inetsrv\appcmd start site /in
```

## Suppression du répertoire virtuel

Afin de supprimer un répertoire vituel d'un site du serveur IIS, il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de supprimer un répertoire vituel d'un site du serveur IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd delete vdir /app.name:"Site01 du Module SRW"  /path:/test1 /physicalPath:C:\SRW\repvirtuel
```

## Sauvegarde de la configuration

Afin de sauvegarder la configuration du serveur IIS, il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de sauvegarder la configuration du serveur IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe add backup "Backup"
```

## Suppression du site Site02

Afin de supprimer un site du serveur IIS il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de supprimer un site du serveur IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd remove site /name:"Site02 du Module SRW"
```

## Restauration de la sauvegarde d'IIS

Afin de restaurer la configuration du serveur IIS en ayant fait un backup précédemment, il faut tout d'abord être connecté en tant qu'adminsitrateur.

Puis, il faut presser sur les touches Windows et R simultanément, saisir powershell et presser sur la touche Enter.

Il faut ensuite saisir la commande suivante afin de restaurer la configuration du serveur IIS.

```Powershell
C:\Windows\system32\inetsrv\appcmd.exe restore backup "Backup"
```

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://fr.wikipedia.org/w/index.php?title=Internet_Information_Services&oldid=140016257, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/install/installing-iis-85/installing-iis-85-on-windows-server-2012-r2, consulté le 30.01.2018.

http://www.tomsitpro.com/articles/powershell-manage-iis-websites,2-994.html, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/application-frameworks/install-and-configure-php-on-iis/install-and-configure-php, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/powershell/module/webadminstration/set-webhandler?view=winserver2012-ps&viewFallbackFrom=winserver2012r2-ps, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/powershell/module/webadministration/new-webftpsite?view=win10-ps, consulté le 30.01.2018.

https://blogs.iis.net/bills/how-to-backup-restore-iis7-configuration, consulté le 30.01.2018.

https://docs.microsoft.com/en-us/iis/get-started/getting-started-with-iis/create-a-web-site, consulté le 2 février 2018

\newpage

# Annexes

Ment.
